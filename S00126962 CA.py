import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy import stats
from sklearn import linear_model
plt.style.use('ggplot')



#function to read the csv file and return the dataframe
def ReadSteamDataCSV():
    data = pd.read_csv('games-features.csv');
    return data;
      
steamData = ReadSteamDataCSV() #already want this data ready when running,so set it here

#Function to pull out a spefic row 
def PrintSpecifcColoum(col_index):
    print(steamData.iloc[col_index]);

#Function to check that data types in each coulm,so we can tell what we need to go into and mend
def CheckColDataTypes():
    print(steamData.dtypes);

#there are a few entires were it will say something like "Coming soon" or Early 20XX
#Replace them with Nan in order to get pands to ingore them,this was only to test a way around this problem
#Found a better soultion below,this was just a test
def RemoveNonDates():
    steamData['ReleaseDate'].replace("Early 2017",np.NaN);
    steamData['ReleaseDate'].replace("Coming Soon",np.NaN);
    print(steamData['ReleaseDate']);
    
#since we dont care about the month,just pull out the year
def ConvertRealseDateToYear():
    steamData['ReleaseDate'] = steamData['ReleaseDate'].astype(str).str[-4:].astype(int,errors="ignore");
    
#currenlty,some of the Current types in the priceType coloum as null,all are shown is USD however
#I am assuming they are all in USD,and some are just null,so just replace null with USD
#Grand scheme of things,this should not be needed,as all the prices are down as USD,so no real need for the label
def FillInPricewithUSD():
    steamData['PriceCurrency'] = steamData.PriceCurrency.replace(np.NaN, 'USD');

#gets a count for the amount of null values in each colloum
def CheckColloumsForNulls():
    print(steamData.isnull().sum());
  

#function to get the index of games that are of a genre type
def GetAllGamesInGenre(genre_Name):
    indexs =steamData.loc[steamData[genre_Name] == True];
    return indexs;

#function to get the index of games that are of a genre type,this time using a sample of the population
def GetAllGamesInGenreSample(genre_Name,sampleSize):
    sampleData = steamData.sample(n=sampleSize); #use pandas sample function to get a random sample
    indexs =sampleData.loc[sampleData[genre_Name] == True];
    return indexs;

#Function to get all the indexs of a game genre in a given year
#can use this to get more specifc data by year,useful for looking at 2016 for example
#well,if it worked.this is the cultipt for all the years functions not work,for the life of me I cant get it to work

def GetAllGamesInGenreByYear(genre_Name,_year):
    genreIndexs =steamData.loc[(steamData[genre_Name] == True)].index;#get the indexs of the genres
    ConvertRealseDateToYear(); # convert the release date to just years before getting the year indexs
    yearIndexs = [idx for idx in steamData["ReleaseDate"].index if _year in steamData["ReleaseDate"]]; # now get a list of all the year indexs for the passed year
    combinedIndexs = [i for i in genreIndexs if i in yearIndexs];
    return combinedIndexs;


#games that did not get a review on metacritc has a default of 0
#this throws off the mean by alot,so replace them with NAN to make pands ingore it inthe calucation
#Not used in code,this is just to test
def ReplaceZeroinMeatricColoums():
    steamData['Metacritic'] = steamData.Metacritic.replace(0,np.NaN);
    print(steamData['Metacritic']);

#Comapre all the metacritic scores of every genre
def CompareMetacritcOfGenres():   
    genreCols = [col for col in steamData if 'GenreIs' in col]; #get all the Genres in a list
    genreMeans = []; #create a empty list to use for means
    x = len(genreCols); #get the count of all the genres for the loop
    
    #now loop to get the the genre,and then calcuate its mean
    for i in range(0,x):
        colName = genreCols[i];
        col = GetAllGamesInGenre(colName); #get the genre we are currenlty looping on
        col = col['Metacritic'].replace(0,np.NaN); #make sure we exclude any 0's,pands will ignore nans when calcuting mean
        mean =col.mean();
        genreMeans.append(mean);
      
    #now lets plot the graph
    plt.figure(figsize=(10, 5)); 
    plt.barh(genreCols,genreMeans,align = 'center'); 
    plt.xticks([0,10,20,30,40,50,60,70,80,90,100]); #set the ticks to intervals of 20
    plt.xlabel('Meatric scores') 
    plt.ylabel('Genres');
    plt.title('Metacritcs scores across all Generes on steam');
    plt.show(); #fianlly show the graph


#Comapre the mean prices of each genre
def ComparePricesOfGenres(finialPrice): 
    genreCols = [col for col in steamData if 'GenreIs' in col] 
    
    searchTerm = "PriceFinal"#default to this
    if finialPrice == False : #if false
        searchTerm = "PriceInitial" #change to Initial
        
    x = len(genreCols) #get the count of all the genres
    priceMeans = [] #create a empty list to use
    #now loop to get the the genre,and then calcuate its mean
    for i in range(0,x):
        colName = genreCols[i];
        col = GetAllGamesInGenre(colName);
        col = col[searchTerm] ;
        mean =col.mean();
        priceMeans.append(mean);
        
    #now lets plot the graph
    plt.figure(figsize=(10, 5)) ;
    plt.barh(genreCols,priceMeans,align = 'center') ;
    plt.xticks(range(0,100,10)) #set the ticks to intervals of 10
    plt.xlabel('Mean' + " " + searchTerm[5:] + " " + 'Prices in USD') ;
    plt.ylabel('Genres') 
    plt.title('Mean' + " " + searchTerm[5:] + " " +  'Prices across all Generes on steam')  ;
    
    #add text to see the values 
    for i, v in enumerate(priceMeans):
        plt.text(v, i, "$"+str(round(v,2)), color='blue', fontweight='bold')    

  
#Lets see what the ownership of games is like per genre
#This will tell us what the most created game genre is
def CompareOwnersPerGenre():
    #we need a smaple of the population for this one
    sampleSize = 5000; #get a large sample size to try and get simliar results to the population
    sampleData = steamData.sample(n=sampleSize)
    genreCols = [col for col in sampleData if 'GenreIs' in col]
    x = len(genreCols) #get the count of all the genres
    genreOwnership = [] #create a empty list to use
    
    #now loop to get the the genre,and then calcuate its sum
    for i in range(0,x):
        colName = genreCols[i];
        col = GetAllGamesInGenreSample(colName,sampleSize);
        col = col['SteamSpyOwners'];
        sumOfOwners =col.sum();
        genreOwnership.append(sumOfOwners);
        
    #now lets plot the graph
    plt.figure(figsize=(15, 5)) #give it a decent size
    plt.barh(genreCols,genreOwnership);
    #cant get the xticks to work
  #  plt.xticks([0,2000000,40000000,60000000,80000000,100000000],["0","2Mil", "4Mil", "6Mil", "8Mil","10Mil"])
    plt.xticks(range(0,6,1),["0","2Mil", "4Mil", "6Mil", "8Mil","10Mil"]);
    plt.xlabel('Owners Per Genre',labelpad=10); 
    plt.ylabel('Genres');
    plt.title('Owners across all Generes on steam') ;          
    
    #add text to make the graph make sense
    for i, v in enumerate(genreOwnership):
        plt.text(v, i, " "+str(v), color='blue', fontweight='bold');  
        
    plt.show() #fianlly show the grapth


#Lets see what year has the most released game
def CompareRealseYears():
     ConvertRealseDateToYear(); #call function to get just years
     y = pd.value_counts(steamData["ReleaseDate"].values,sort =False); #get a count of the years
     df = pd.DataFrame({'Dates' : y.index, 'CountOfRelease' : y.values}); #create a new dataframe to hold the count and year
     df =df[df.Dates.apply(lambda x: x.isnumeric())]; #some of the data is still along the lines of "coming soon ect",only care about years
     df =df.sort_values(by=["Dates"]); #sort them by the year
     
     #now lets make chart with the data
     labels = df["Dates"];
     count = df["CountOfRelease"];
     plt.figure(figsize=(10, 10)); 
     plt.title("Count of Games released on Steam by Year");
     #label both of the axis
     plt.ylabel("Years");
     plt.xlabel("Number of Games released");
     plt.barh(labels,count);
     
     #loop though and add text to the end of each chart,makes it easier to tell what numbers are what
     for i, v in enumerate(count):
        plt.text(v, i, " "+str(v), color='blue', fontweight='bold')    
     plt.show();#show the graph
     

#2016 has a disportenial amount of years released
#so lets make a function so we can look at that more clearly
#Was unable to get this to work,result is the same as the overall version,including for completness sake      
def ComparMetaCriticGenreInYear(_year):
    genreCols = [col for col in steamData if 'GenreIs' in col];
    x = len(genreCols); #get the count of all the genres
    genreOwnership = []; #create a empty list to use
    
    #now loop to get the the genre,and then calcuate its mean
    for i in range(0,x):
        colName = genreCols[i];
        indexes = GetAllGamesInGenreByYear(colName,_year);
        col = steamData.loc[indexes];
        col = col['Metacritic'].replace(0,np.NaN); #make sure we exclude any 0's,pands will ignore nans when calcuting mean        
        mean =col.mean();
        genreOwnership.append(mean);
        
    #now lets plot the graph
    plt.figure(figsize=(15, 5)); #give it a decent size
    plt.barh(genreCols,genreOwnership) ;
    plt.xticks([0,10,20,30,40,50,60,70,80,90,100]) ;
    plt.xlabel('Metacritc Scores',labelpad=10);
    plt.ylabel('Genres') ;
    plt.title('MetaCrtic Scores across all Generes on steam in the year' + " " + str(_year))           
    
   
#Same idea as above,just want to look into the year closer
#Was unable to get this to work,result is the same as the overall version,including for completness sake              
def CompareOwnersPerGenreInYear(_year):
    genreCols = [col for col in steamData if 'GenreIs' in col]
    x = len(genreCols) #get the count of all the genres
    genreOwnership = [] #create a empty list to use
    
    #now loop to get the the genre,and then calcuate its sum
    for i in range(0,x):
        colName = genreCols[i];
        indexes = GetAllGamesInGenreByYear(colName,_year);
        col = steamData.loc[indexes];
        col = col['SteamSpyOwners'];       
        mean =col.sum();
        genreOwnership.append(mean);
        
    #now lets plot the graph
    plt.figure(figsize=(15, 5));
    plt.barh(genreCols,genreOwnership);
  #  plt.xticks([0,2000000,40000000,60000000,80000000,100000000],["0","2Mil", "4Mil", "6Mil", "8Mil","10Mil"])
   # plt.xticks(range(0,6,1))
    plt.xlabel('Owners per genre',labelpad=10);
    plt.ylabel('Genres');
    plt.title('Owners across all Generes on steam in the year' + " " + str(_year));         
    
    for i, v in enumerate(genreOwnership):
        plt.text(v, i, " "+str(v), color='blue', fontweight='bold'); 

#Again same idea,trying to get more information on a yearly basis
#Was unable to get this to work,result is the same as the overall version,including for completness sake      
def ComparePricesOfGenresByYear(_year): 
    genreCols = [col for col in steamData if 'GenreIs' in col];
    
    x = len(genreCols); #get the count of all the genres
    priceMeans = []; #create a empty list to use
    #now loop to get the the genre,and then calcuate its mean
    for i in range(0,x):
        colName = genreCols[i];
        indexes = GetAllGamesInGenreByYear(colName,_year);
        col = steamData.loc[indexes];
        col = col['PriceFinal'];
        mean =col.mean();
        priceMeans.append(mean);
        
    #now lets plot the graph
    plt.figure(figsize=(10, 5)); 
    plt.barh(genreCols,priceMeans,align = 'center');
    plt.xticks([0,20,40,60,80,100]); #set the ticks to intervals of 20
    plt.xlabel('Mean Final Prices'); 
    plt.ylabel('Genres');
    plt.title('Mean Final Prices across all Generes on steam'); 
    
    for i, v in enumerate(priceMeans):
        plt.text(v, i, "$"+str(round(v,5)), color='blue', fontweight='bold');   


#I want to see how many game exists in each genre  
def CompareNumberOfGenres():   
    genreCols = [col for col in steamData if 'GenreIs' in col] 
    genreCount = [] #create a empty list to use
    totalCount =0; #create a var to add up a total of every game,might be nice to show
    x = len(genreCols) #get the amount of geners in the list
    
    #now loop to get the the genre,and then get the amount of items(using indexs,proably other ways)
    for i in range(0,x):
        colName = genreCols[i];
        col = GetAllGamesInGenre(colName);
        count =len(col.index);
        genreCount.append(count);
        totalCount += count; #keep adding up the total though the loop
      
    #now lets plot the graph
    plt.figure(figsize=(10, 5)); 
    plt.barh(genreCols,genreCount,align = 'center');
    plt.xlabel('Count of each game in genre'); 
    plt.ylabel('Genres'); 
    #it should be noted some games will be counted twice,this value is just the amount of items being counted for the graph
    plt.title('Count of all games across all Generes on steam\n' + "Total Games Counted:" + str(totalCount));

#same as above,but limit it by year this time,for a better look at 2016 mainly
#Also not working,cant seem to get games by year out,including for completness sake
def CompareNumberOfGenresbyYear(_year):
    genreCols = [col for col in steamData if 'GenreIs' in col] 
    genreCount = [] #create a empty list to use
    totalCount =0; #create a var to add up a total of every game,might be nice to show
    x = len(genreCols) #get the amount of geners in the list
    
    #now loop to get the the genre,and then get the amount of items(using indexs,proably other ways)
    for i in range(0,x):
        colName = genreCols[i];
        indexes = GetAllGamesInGenreByYear(colName,_year)
        col = steamData.loc[indexes];
        count =len(col.index)
        genreCount.append(count);
        totalCount += count;
      
    #now lets plot the graph
    plt.figure(figsize=(10, 5));
    plt.barh(genreCols,genreCount,align = 'center');
    plt.xlabel('Count of each game in genre');
    plt.ylabel('Genres');
     #it should be noted some games will be counted twice,this value is just the amount of items being counted for the graph
    plt.title("Count of all games across all Generes on steam in" + " " + str(_year) + "\n" + "Total Games Counted:" + str(totalCount))#give it a title

#I want a way to see how many people recommended a game
#I am assumning that anyone that recommended the game owns the game here
#Going to do this by genre,So I can try and get some information for regession
def CompareGameRecommendations():
    genreCols = [col for col in steamData if 'GenreIs' in col]
    x = len(genreCols) #get the count of all the genres
    recomendRaitingList = [] #create a list to store the rating
    for i in range(0,x):
        colName = genreCols[i]
        col = GetAllGamesInGenre(colName)
        owners = col["SteamSpyOwners"];
        recomendations = col["RecommendationCount"];
        #get the recomndations as a percent,this isnt perfect since not all people that own a game will
        #get feedback,but its the best raiting I can come up with given the data avabile
        recomendationForGenre = (recomendations.sum()/owners.sum()) *100
        recomendRaitingList.append(recomendationForGenre)
        
    plt.figure(figsize=(10, 5)) #give it a decent size
    plt.barh(genreCols,recomendRaitingList,align = 'center'); 
    plt.xlabel('Percentage of owners recommendations'); 
    plt.ylabel('Genres'); 
    plt.title("Percent of owners that recommadtioned games based on Genre");#give it a title
    
    for i, v in enumerate(recomendRaitingList):
        plt.text(v, i, " "+str(round(v,2)) + "%", color='blue', fontweight='bold') 

#Want to see if there is a correlation between the starting and end price
#Not used,another version by genre exists below
def CoerilateOrginalandFinalPrice():
    initPrice = steamData["PriceInitial"]
    finalPrice = steamData["PriceFinal"]
    corr = np.corrcoef(initPrice,finalPrice)
    plt.scatter(initPrice, finalPrice)
    plt.show();
    print(corr);

#Not used in the end up,more compelte function is below
def CoerilateOwnserShipAndPrice():
    ownerShip = steamData["SteamSpyOwners"];
    finalPrice = steamData["PriceFinal"];
    corr = np.corrcoef(ownerShip,finalPrice);
    plt.scatter(ownerShip, finalPrice);
    plt.show();
    print(corr);
    
#A while back,there use to be abit of a racket going on with steam cards given out by achivements
#People would buy games that gave out alot of these cards via achiements,and the acvihments were stuff like "Play the game" ect
#Lets see if that have any effect on ownership
def CoerilateAchivmentsAndOwership():
    sampleAchiments = steamData["AchievementCount"]
    sampleOwners = steamData["SteamSpyOwners"]
    corr = np.corrcoef(sampleAchiments,sampleOwners) 
    plt.figure(figsize=(20, 5)) 
    plt.scatter(sampleOwners, sampleAchiments) 
    plt.show(); #show the graph
    print(corr); #print out the coleration
    
#not much data to be found here,tryed action first but decided
#I might as well make charts for all of these
#This is the chart version I created,Not used in the report,but if you want to see the charts you can use this
def CoerilateOwnserShipAndPriceByGenre():
    genreCols = [col for col in steamData if 'GenreIs' in col]
    df = pd.DataFrame(); #tryed to create a dataframe so I can show this in a scater maxtirx,left code in below
    x = len(genreCols) #get the length of the genres
    for i in range(0,x): #now loop though and get the correlation for each
        colName = genreCols[i]
        col = GetAllGamesInGenre(colName); 
        ownerShip = col["SteamSpyOwners"];
        finalPrice = col["PriceFinal"];
        corr = np.corrcoef(ownerShip,finalPrice) #get the correlation
        plt.scatter(ownerShip, finalPrice) #use a scatter for this
        plt.title("Correlation between ownership and price for\n" + colName); #use the colname to title each
        plt.xlabel("Owners");
        plt.ylabel("Final Price");
        plt.show(); #show the graph
        print(corr); #print out the colleration below each graph
      
    #Now do the same for the entire dataset
    ownerShip = steamData["SteamSpyOwners"]; #get the ownership
    finalPrice = steamData["PriceFinal"]; # get the final pruce
    corr = np.corrcoef(ownerShip,finalPrice); #get the correlation
    df["Overall"] = pd.Series(ownerShip,finalPrice);
    plt.scatter(ownerShip, finalPrice); #use a scatter for this
    plt.title("Correlation between ownership and price for\n All genres combined"); #use the colname to title each
    plt.xlabel("Owners"); 
    plt.ylabel("Final Price"); 
    plt.show();
    print(corr); #print out the colleration below each graph

        
    

#see if there is any significant change between game costs from realse to the final price
#this function will return a test based on each genre and then an overall test  
#This is an indepdance test
def HypoTestDiffernceBetweenPrices():
    genreCols = [col for col in steamData if 'GenreIs' in col]
    x = len(genreCols) #get the count of all the genres
    pValList = [] # use this to store the p values for a chart
    for i in range(0,x):
        colName = genreCols[i]
        col = GetAllGamesInGenre(colName)
        initPrices = col["PriceInitial"];
        finalPrices = col["PriceFinal"];
        pVal = stats.ttest_ind(initPrices, finalPrices).pvalue; #get the pVal
        pValList.append(pVal) #append it to the list for later
        print("P val for" + " " + colName + ":" + " " + str(pVal));
        DefineHypoPVal(pVal,colName);#define the test result using this function
        
    #now get the overall outside of the loop
    initPrices = steamData["PriceInitial"];
    finalPrices = steamData["PriceFinal"];
    pVal = stats.ttest_ind(initPrices, finalPrices).pvalue
    pValList.append(pVal);
    genreCols.append("AllGenres")
    print("P Val val for all genres combied:" + " " + str(pVal))
    DefineHypoPVal(pVal,"AllGenres")
    
    plt.figure(figsize=(10, 5)) 
    plt.barh(genreCols,pValList,align = 'center'); 
    plt.xlabel('P Values for indepdant T test'); 
    plt.ylabel('Genres');
    plt.xticks(np.arange(0, 1, step=0.1));
    plt.title("P vals to see if there is a major difference between the starting and final price")
    
    for i, v in enumerate(pValList):
        plt.text(v, i, " "+str(round(v,4)), color='blue', fontweight='bold')  
    

#I wanted to see if there metacritic scores reflect what the users say about the game
#This is an indepdance test
def HypoTestMetaCriticAndUserRaiting():
    genreCols = [col for col in steamData if 'GenreIs' in col]
    x = len(genreCols) #get the count of all the genres
    metaList = [] #list to store metacritic scores
    userRatingList = [] #list to store user raiting
    for i in range(0,x):
        colName = genreCols[i]
        col = GetAllGamesInGenre(colName);
        metacritic = steamData["Metacritic"].mean(); #get the mean for the genre(will be around 70-80 for all)
        metaList.append(metacritic) ##append it to the list
        #the rating is the sum of the recomendation over the amount of owners by 100 to get a precent
        #since metacritic scores are whole numbers,multpiy by 100 again to get it as a whole number(0.7 -> 70)
        recomendationForGenre = ((col["RecommendationCount"].sum()/col["SteamSpyOwners"].sum()) *100) *100
        recomendationForGenre = round(recomendationForGenre,0) # finaly round it to 0 places to better match meta scores
        userRatingList.append(recomendationForGenre)
        
#now run the test once we have all the data we need for the test
    pVal = stats.ttest_ind(metaList, userRatingList).pvalue;
    genreCols.append("AllGenres");
    print("P Val val for all genres combied:" + " " + str(pVal));
    DefineHypoPVal(pVal,"AllGenres")
    
    
      
#Function I use to accept/reject the results of the indpedance tests     
def DefineHypoPVal(pVal,colName):
     if pVal >= .05: #if its above .5, cant rejectreject
            print("We cannot reject the Null Hypothesis");
            print("There is no tangilbe differnce between the Inital and Final Price for" + " " +colName + "\n");
     else: #otherwise we can reject
           print("We can reject the Null Hypothesis");
           print("There is a tangilbe differnce between the Inital and Final Price for" + " " +colName + "\n");
 
#helper function to print out details about the correlations
#I am doing alot of looping with the coerlations,so makes sense to get another function
#to do the printing for all of them
def DefineColerationAndPrint(corrVal,ColName):
    #first I need to find out if its a postive/negative correlation
    corrType = "";
    if(corrVal >0): # if its above 0,its postive
        corrType = "Positive";
    elif(corrVal < 0): #if its below 0,its negative
        corrType = "Negative";
       
    #Now just to though and find out the strength of the correlation
    print("(" + corrType + " correlation)" + " " + "np.corroef output val for" + " " + ColName + ":" + str(corrVal))
    if (abs(corrVal) ==0): # I already know if its postive or negative,so use absuolte function to avoid having to blocks to figure out the corr type
            print("No correlation for" + " " + ColName);
    elif(abs(corrVal) > 0  and abs(corrVal) < 0.2) :
            print("Very weak correlation for" + " " + ColName+ "\n");
    elif(abs(corrVal) > 0.2  and abs(corrVal) < 0.4) :
            print("Weak correlation for" + " " + ColName+ "\n");
    elif(abs(corrVal) > 0.41  and abs(corrVal) < 0.7) :
            print("Moderate correlation for" + " " + ColName+ "\n");
    elif(abs(corrVal) > 0.71  and abs(corrVal) < 0.9) :
          print("Strong correlation for" + " " + ColName+ "\n");
    elif(abs(corrVal) > 0.9) :
        print("Very Strong correlation for" + " " + ColName + "\n");
            

#More complete function from before
#Again this does the test by genre and then overall
#This is the one I ended up using
def CorelateOwnersAndMetatric():   
    genreCols = [col for col in steamData if 'GenreIs' in col]
    if "GenreIsNonGame" in genreCols: genreCols.remove("GenreIsNonGame"); # Get rid of non game from the list
    x = len(genreCols); #get the count of all the genres
    corrValList = []; # use this to store the corr values 
    for i in range(0,x):
        colName = genreCols[i];
        col = GetAllGamesInGenre(colName);
        metaCritc = col["Metacritic"];
        owners = col["SteamSpyOwners"];
        corr = np.corrcoef(metaCritc,owners); #get the value
        corrValList.append(corr.item(1)); #get the reuslt out of the matrix,makes it easier to show latter
        DefineColerationAndPrint(corr.item(1),colName);#send result off to be defined    
        
    #now get the overall outside of the loop
    metaCritc = steamData["Metacritic"];
    owners = steamData["SteamSpyOwners"];
    corr = np.corrcoef(metaCritc,owners);
    corrValList.append(corr.item(1));
    genreCols.append("Overall"); # need to add on another item here to account for the extra val
    DefineColerationAndPrint(corr.item(1),"Overall"); # call the function to define the correlation
    
    #I never end up using the graph in the document,you can look at it if you want
    #nothing really to gleam for it though,so left it out
    plt.figure(figsize=(30,5)); #give it a decent size
    plt.scatter(corrValList,genreCols); #using a horisontal graph
    plt.xlabel('P Values for indepdant T test'); 
    plt.ylabel('Genres'); 
    plt.title("Corr values per genre for correlations between owners and meta scores");
    
    for i, v in enumerate(corrValList):
        plt.text(v, i, " "+str(round(v,4)), color='blue', fontweight='bold'); 
        
        

#try and correlate price and dlc
def CorelatePriceAndDLC():   
    genreCols = [col for col in steamData if 'GenreIs' in col]
    if "GenreIsNonGame" in genreCols: genreCols.remove("GenreIsNonGame")
    x = len(genreCols) #get the count of all the genres
    corrValList = [] # use this to store the p values for a chart
    for i in range(0,x):
        colName = genreCols[i]
        col = GetAllGamesInGenre(colName)
        dlcCount = col["DLCCount"];
        finalPrice = col["PriceFinal"];
        corr = np.corrcoef(dlcCount,finalPrice)
        corrValList.append(corr.item(1));
        DefineColerationAndPrint(corr.item(1),colName)     
        
    #now get the overall outside of the loop
    dlcCount = col["DLCCount"];
    finalPrice = col["PriceFinal"];
    corr = np.corrcoef(dlcCount,finalPrice);
    corrValList.append(corr.item(1));
    genreCols.append("AllGenres");
    genreCols.append("Overall"); # need to add on another item here to account for the extra val
    DefineColerationAndPrint(corr.item(1),"Overall");
    
def CorelatePriceAndOwnerShip():   
    genreCols = [col for col in steamData if 'GenreIs' in col]
    if "GenreIsNonGame" in genreCols: genreCols.remove("GenreIsNonGame")
    x = len(genreCols) #get the count of all the genres
    corrValList = [] # use this to store the p values for a chart
    for i in range(0,x):
        colName = genreCols[i]
        col = GetAllGamesInGenre(colName)
        owners = col["SteamSpyOwners"];
        finalPrice = col["PriceFinal"];
        corr = np.corrcoef(owners,finalPrice)
        corrValList.append(corr.item(1));
        DefineColerationAndPrint(corr.item(1),colName)     
        
    #now get the overall outside of the loop
    owners = col["SteamSpyOwners"];
    finalPrice = col["PriceFinal"];
    corr = np.corrcoef(owners,finalPrice);
    corrValList.append(corr.item(1));
    genreCols.append("AllGenres");
    genreCols.append("Overall") # need to add on another item here to account for the extra val
    DefineColerationAndPrint(corr.item(1),"Overall");
    
   
#I want to try and predict Some stuff,eg if I realse a game with Certain varibles,can I predict how many users it will have
#Here I want to see if I can gauge how many people with get the game
def GamePredictionUsingRegression(DlcCount,PriceFinal,GenreIs,AchievementCount,indieTest):
    clf = linear_model.LinearRegression() ##get the modal to use out of sk learn
    #set data I want to use in the regression
    clf.fit(steamData[["DLCCount","PriceFinal",GenreIs,"AchievementCount","GenreIsIndie"]],steamData.SteamSpyOwners) 
    #now pass in the values I want to predict
    predictArray =clf.predict([[DlcCount,PriceFinal,True,AchievementCount,indieTest]]) #predict the game passed in,and then
    revenue = predictArray[0] * PriceFinal # get the total revenue
    #round the figures to actual numbers,cant have .55 of a user
    print("Expected Users:" + str(round(predictArray[0],0)) +"\n" + "Expected Revenue:" + str(round(revenue,2)) + "\n" )


#function to quickly run all the predection I want to try and Make
def RunRetroDuckPredecits():
    print("Greedy Knight:")
    GamePredictionUsingRegression(1,2,"GenreIsAction",20,True);
    print("Project Mallard:")
    GamePredictionUsingRegression(2,5,"GenreIsStrategy",50,True);
    print("Sarcastic Knight:")
    GamePredictionUsingRegression(4,15,"GenreIsRPG",100,True);
    
    
    
    
    
